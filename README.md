# README

## Table of contents

- [Introduction](#introduction)
- [Implemented design patterns](#design-patterns)
- [Instructions to use, test and deploy](#instructions)

## <a name="introduction"></a>Introduction

I'm an experienced java architect who fell in love with rails, although I know how things should be implemented, I'm still learning the 'rails way' to implement. So, in this app I've made an experiment using a few software design patterns to solve the problem proposed here: https://github.com/pipefy/RecruitmentExercise/blob/master/BACKENDEXERCISE.md

## <a name="design-patterns"></a>Implemented design patterns

**Abstract factory design pattern** the dashboard view presents data of three different models therefore, to let the dashboards controller skinny, I used this pattern to abstract the complexity of the models. This pattern separates the details of implementation of a set of objects from their general usage and relies on object composition, as object creation is implemented in methods exposed in the factory interface. The  pattern was implemented using a Rails Active Model class. This solution abstracts the implementations details of the models creating a single point to be altered (instead of all views) if the models have changes.

**Adapter design pattern** all the calls to the Pipefy api are encapsulated in the adapters creating a single point to be altered if the api changes. The app doesn't no details of the api implementation. The only exception is the code which handles the cards, because choose to persist the data as a jsonb field in the database. This strategy creates a bigger dependency of the API but substantially reduces the complexity of the database.  

**Facade design pattern** the service to the feature fecht new data a facade which encapsulates all the calls to the adapters. This solution improves the maintainability of the code because creates a unique point to be altered if the adapter changes. This is very important specially in this case, since there is a third party involved (Pipefy API).  

**Chain of responsibility design pattern** Requests to the Pipefy API are handled by a chain of responsibility inside the adapter. The query is prepared by the Pipefy adapter which passes to the base adapter, create and execute the request post to the API. Then the response is handled by the Pipefy adapter which delegates the json to be parsed by the parse api adapter. Finally the presenter adapter format the data to be used outside the adapters. Only the base adapter execute requests.

## <a name="instructions"></a>Instructions to use, test and deploy

The development and test environments use the Fake Pipefy API local server. To run this server with the rails app start the services using foreman with the Procfile.development file (foreman start -f Procfile.development). When testing, this serve is automatically started with the rspec command.

In production environment the app points to the real API url, so it's necessary to seed the database with a valid organization id and acess token and a valid pipe. Create a file named production.rb inside db/seeds and run rake db:seed RAILS_ENV=production.

To point the others environments to the real Pipefy API you need to change the figaro gem config file config/application.yml and seed the database with a real organization and pipe attributes. 