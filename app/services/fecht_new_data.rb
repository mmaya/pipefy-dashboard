#This service is a facade to encapsulate all the calls to the adapter. This solution improves the maintainability of the code because creates a unique point to be altered if the adapter changes. This is very important specially in this case, since there is a third party involved (Pipefy API).  
class FechtNewData
  def initialize (object, object_id, acess_token)
    @object = object
    @object_id = object_id
    @pipefy_api_token = acess_token
  end

  def execute
    fecht_new_data
  end

  private
  
  def fecht_new_data
    PipefyAdapters::PipefyAdapter.new(@pipefy_api_token).query_object(@object, @object_id)

    # the generic method insure the autonomy of this service, note that there is no business logic of dashboard here. This insure the unilateral flux of information: dashboard controller knows only the service logic, which knows only the adapter logic that knows only the API logic. 
  end 
  
end
