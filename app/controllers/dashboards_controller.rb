class DashboardsController < ApplicationController
  before_action :set_dashboard
  
  def index
    @data_partial = set_dashboard
  end
  
  def fecht_new_data
    @dashboard.organization_set_name (FechtNewData.new('organization', @dashboard.organization_pipefy_id, @dashboard.organization_pipefy_api_token).execute)

    @dashboard.pipe_set_name(FechtNewData.new('pipe', @dashboard.pipe_pipefy_id, @dashboard.organization_pipefy_api_token).execute)

    @dashboard.card_list_set_cards(FechtNewData.new('cards', @dashboard.pipe_pipefy_id, @dashboard.organization_pipefy_api_token).execute)

    @dashboard.save
    
    redirect_to root_path
    flash[:success] = "All data is up to date!"
  end
  
 private
 
  def set_dashboard
    begin
      @dashboard = Dashboard.new
      'pipefy_data'
    rescue Error::ApiDataError => error
      $stderr.puts error.to_s 
      $stderr.puts error.backtrace
      flash[:error] = ('Something went wrong: There is no data saved to show.')
      'no_data_found'
    end
  end
  
end