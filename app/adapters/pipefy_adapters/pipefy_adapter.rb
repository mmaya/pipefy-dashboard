# This module is responsible for orchestrate the request and the respective response. It selects the correct query and delegates to the base adapter, then it sends the response to be parsed (ParseApiResponse adapter) and formatted (AdapterDataPresenter).  
module PipefyAdapters
  class PipefyAdapter
    require 'rest_client'

    def initialize (acess_token)
      @pipefy_acess_token = acess_token
    end

    #To do: change this logic so it can treat more than one query per object. Find an elegant way to query by name.
    def query_object(object, object_id)
      query = GraphQlQuery.find_by(object:object)
    if !query.nil?
      value = query.value.gsub('#', object_id.to_s)
    else
      raise Error::ApiResponseError
      puts 'erro'
    end
    json = parse_api_data (value)
    AdapterDataPresenter.execute(object, json)
    end


private
    def parse_api_data(value)
      response = BaseAdapter.execute_request(@pipefy_acess_token, value)
ParseApiResponse.new(response).parsed_json
    end

  end
end