module PipefyAdapters
  class AdapterDataPresenter
  
    def self.execute (object, json)
      case object
      when "organization" then  return json["data"]["organization"]["name"]
      when "pipe" then return json["data"]["pipe"]["name"]
      when "cards" then return json
      end
    end
  end
end