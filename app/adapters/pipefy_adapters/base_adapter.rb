module PipefyAdapters
require 'rest_client'
  class BaseAdapter
    include ApplicationHelper
    cattr_accessor :base_url, :request

      self.base_url = ENV.fetch("PIPEFY_API_BASE_URL")

    def self.execute_request(pipefy_acess_token, values)
      begin
      
      headers = {:content_type => 'application/json', :authorization => 'Bearer ' + pipefy_acess_token}
      response = self.request = RestClient.post self.base_url, values, headers
  response.code == 200 or raise Error::ApiResponseError
        response
      end
    end
  end
end
