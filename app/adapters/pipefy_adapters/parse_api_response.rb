module PipefyAdapters

  class ParseApiResponse
    attr_reader :response_body, :parsed_json

    def initialize(response_body)
      @response_body = response_body
      @parsed_json = parse_response_body
    end

    private

      def parse_response_body
        begin
          JSON.parse(@response_body).key?('data') ? @parsed_json = JSON.parse(@response_body) : raise(Error::ApiResponseError) 
      rescue JSON::ParserError => e
        raise Error::ApiResponseError
        end
      end
  end
end