    # The list of cards could be persisted with the pipe owner as a jsonb field because this app is too simple, but I choose to create a separated model so the app can grow and accommodate more attributes like the labels or phase history. 
    # The problem with store the raw api data as a jsonb field is create a code dependency outside the adapter. Nevertheless, it substantially reduces the complexity of parsing and modeling thirf party data. To conclude, there is "no silver bullet"."
class CardList < ApplicationRecord
  belongs_to :pipe, foreign_key: 'pipe_id'
  
  validates :pipe_id, :presence => true, :uniqueness => true, :numericality => {:only_integer => true}

  def cards
    CardList.select("*").select("jsonb_extract_path(cards, 'data', 'cards', 'edges') as card_list ").find_by(pipe:pipe).card_list
  end

  def card_fields (card_pipefy_id)
#     To do: put the phase fields in a modal using this query
    CardList.find_by_sql("WITH PIPE_CARDS AS (SELECT jsonb_array_elements(jsonb_extract_path(cards, 'data', 'cards', 'edges')) as node_cards FROM card_lists WHERE pipe_id=?) SELECT jsonb_extract_path(node_cards, 'node', 'fields') as cards_fields FROM PIPE_CARDS WHERE node_cards @>'{\"node\": { \"id\" : \"?\"}}'::jsonb;", pipe.id, card_pipefy_id)
  end
end
