#Model class created to instantiate and update in a single transaction the three models used on the dashboard view. It reduces the complexity of the dashboard controller and ensures data integrity since the three models are going to be simultaneously updated with the api data. 
class Dashboard
  include ActiveModel::Model

  def initialize
    find_defaults
  end
  
  def find_defaults
    @organization = Organization.default || Organization.first
    raise Error::ApiDataError if @organization.nil? 

    @pipe = @organization.default_pipe || @organization.pipes.first
    raise Error::ApiDataError if @pipe.nil?

    @card_list = CardList.find_by(pipe:@pipe)
    raise Error::ApiDataError if @card_list.nil?
  end
  
  def organization_name
    @organization.name
  end
  
  def pipe_name
    @pipe.name
  end
  
  def organization_set_name(name)
    @organization.name = name
  end
  
  def pipe_set_name(name)
    @pipe.name = name
  end

  def cards
    @card_list.cards
  end
  
  def card_list_set_cards(cards)
    @card_list.cards = cards
  end

def card_fields
  @card_list.card_fields
end

def organization_pipefy_api_token
  @organization.pipefy_api_token
end

def organization_pipefy_id
  @organization.pipefy_id
end

def pipe_pipefy_id
  @pipe.pipefy_id
end

  
  def save
    ActiveRecord::Base.transaction do
      @organization.save!
      @pipe.save!
      @card_list.save!
    end
  end
  
  private
  attr_accessor :organization, :pipe, :card_list

end
