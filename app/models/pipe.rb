class Pipe < ApplicationRecord
  belongs_to :organization, foreign_key: 'organization_id'
  has_one :card_list
  validates :pipefy_id, :uniqueness => true, :numericality => {:only_integer => true}
  
  validates :organization_id, :presence => true, :numericality => {:only_integer => true}

end
