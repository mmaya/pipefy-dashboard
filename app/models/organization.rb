class Organization < ApplicationRecord
  has_many :pipes, dependent: :destroy
  attr_encrypted :pipefy_api_token, key: ENV.fetch("SECRET_KEY_BASE")
  
validates :pipefy_id, :uniqueness => true, :numericality => {:only_integer => true}

    scope :default, -> { find_by(default: true) }

  
  def default_pipe
        Pipe.select("*").where(default: true).where(organization_id: id).take
  end
  
  def has_pipe?(pipe_id)
    Pipe.select("*").where(pipe_id: pipe_id).where(organization_id: id).any?
  end
end
