require 'rails_helper'
 
RSpec.describe CardList, type: :model do
  it "has a valid factory" do
    expect(build_stubbed(:card_list)).to be_valid
  end
  
  let(:card_list) { create(:card_list) }
  
  describe "ActiveModel validations" do
    it { expect(card_list).to validate_numericality_of(:pipe_id).only_integer }
    it { expect(card_list).to validate_uniqueness_of(:pipe_id) }
    it { expect(card_list).to belong_to(:pipe).with_foreign_key('pipe_id') }
  end




end