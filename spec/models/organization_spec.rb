require 'rails_helper'

RSpec.describe Organization, type: :model do
  it "has a valid factory" do
    organization = build_stubbed(:organization)
    expect(organization).to be_valid
  end
  
    let(:organization) { build(:organization) }
  
  describe "ActiveModel validations" do
    it { expect(organization).to validate_numericality_of(:pipefy_id).only_integer }
    it { expect(organization).to validate_uniqueness_of(:pipefy_id) }
    it { expect(organization).to  encrypt(:pipefy_api_token) }
    
  end
end