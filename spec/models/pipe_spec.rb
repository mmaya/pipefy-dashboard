require 'rails_helper'

RSpec.describe Pipe, type: :model do
    it "has a valid factory" do
    pipe = build_stubbed(:pipe)
    expect(pipe).to be_valid
  end
  
    let(:pipe) { create(:pipe) }
  
  describe "ActiveModel validations" do
    it { expect(pipe).to validate_numericality_of(:pipefy_id).only_integer }
    it { expect(pipe).to validate_uniqueness_of(:pipefy_id) }
    it { expect(pipe).to validate_numericality_of(:organization_id).only_integer }
    it { expect(pipe).to belong_to(:organization).with_foreign_key('organization_id')}
  end
end
