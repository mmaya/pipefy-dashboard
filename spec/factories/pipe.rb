FactoryBot.define do
      # Pipe factory with associated with cards
  factory :pipe do
    name { Faker::Lorem.words(2) }
    pipefy_id {rand(3000..5000)}
    organization
      
    after(:stub) do |pipe, evaluator|
        build_list(:card_list, 1, pipe:pipe)
      end
  end
end
