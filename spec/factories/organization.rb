FactoryBot.define do

  # organization factory associated with pipes
  factory :organization do
      name { Faker::Lorem.words(2) }
      pipefy_id {rand(3000..5000)}
      pipefy_api_token = 'foobar22'
      
      #number of pipes to be created and associated
      transient do
        pipes_count 1
      end

      after(:create) do |organization, evaluator|
        create_list(:pipe, evaluator.pipes_count, organization:organization)
      end
  end
end