FactoryBot.define do
  
# CardList factory with a `belongs_to` association for the pipe
  factory :card_list do
    cards {'{"data"=>{"pipe"=>{"id"=>"335557", "name"=>"Back-end Test", "start_form_fields"=>[{"label"=>"What\'s the bug?", "id"=>"what_s_the_bug"}, {"label"=>"Describe this bug", "id"=>"what_is_happening"}, {"label"=>"Attach screenshots of the bug", "id"=>"attach_screenshots_of_the_bug"}], "labels"=>[{"name"=>"Blocker", "id"=>"1317929"}, {"name"=>"Critical", "id"=>"1317930"}, {"name"=>"Function loss (no workaround)", "id"=>"1317931"}, {"name"=>"Function loss (with workaround)", "id"=>"1317932"}, {"name"=>"Cosmetic problem", "id"=>"1317933"}, {"name"=>"Enhancement Request", "id"=>"1317934"}], "phases"=>[{"name"=>"Screening", "fields"=>[{"label"=>"Where is the bug?", "id"=>"where_is_the_bug"}, {"label"=>"How does the bug impact the users?", "id"=>"severity"}, {"label"=>"What\'s the priority?", "id"=>"what_s_the_priority"}], "cards"=>{"edges"=>[{"node"=>{"id"=>"4368600", "title"=>"Missing translation on open card"}}, {"node"=>{"id"=>"4368652", "title"=>"Missing translation on pipe settings"}}]}}, {"name"=>"Bug Backlog", "fields"=>[], "cards"=>{"edges"=>[{"node"=>{"id"=>"4368418", "title"=>"Missing translation on reports"}}]}}, {"name"=>"Queued", "fields"=>[], "cards"=>{"edges"=>[{"node"=>{"id"=>"4368516", "title"=>"Error on moving card"}}]}}, {"name"=>"Validation", "fields"=>[{"label"=>"Bug status", "id"=>"bug_status"}, {"label"=>"Validation details", "id"=>"validation_details"}], "cards"=>{"edges"=>[]}}, {"name"=>"Fixed", "fields"=>[], "cards"=>{"edges"=>[]}}, {"name"=>"Archived", "fields"=>[], "cards"=>{"edges"=>[]}}]}}}'}
    pipe
#     name { Faker::Lorem.words(rand(1..5)) }
#     title { Faker::Lorem.words(rand(1..10)) }
#     pipefy_id {rand(3000..5000)}
#     comments_count {3}
#     comments { [Faker::Hacker.say_something_smart, Faker::Hacker.say_something_smart, Faker::Hacker.say_something_smart] }
#     assignees {[Faker::Name.name_with_middle, Faker::Name.name_with_middle]}
#     current_phase { Faker::Lorem.words(rand(1..3) )}
#     done {rand < 0.5}
#     due_date {Faker::Date.between(1.year.from_now, Date.today)}
#     fields {[Faker::Lorem.words(rand(1..3))=> Faker::Lorem.words(rand(1..5)), Faker::Lorem.words(rand(1..3))=>Faker::Lorem.words(rand(1..5)), Faker::Lorem.words(rand(1..3))=>Faker::Lorem.words(rand(1..5))]}
#     labels {[Faker::Hacker.adjective ,Faker::Hacker.adjective ]}
#     pipefy_url {Faker::Internet.url}
#     pipe
  end
end