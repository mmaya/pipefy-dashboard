require "rails_helper"

RSpec.feature 'View a dasboard', :type => 'feature' do  
  before :each do
    
  end
  
  scenario "User acess the dashboards index" do
    visit "dashboards"
     expect(page).to have_text("Pipefy Recruitment Test")
     expect(page).to have_text("Back-end Test")
     expect(page).to have_text("Something is not working")
     expect(page).to have_text("Missing translation on reports")
  end
  
  scenario "User clicks the 'fecht new data button'" do
    visit "dashboards"
     click_button "Fecht new data"
     expect(page).to have_text("All data is up to date!")
  end

end