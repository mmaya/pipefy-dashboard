require 'spec_helper'
require "rails_helper"

RSpec.describe "the Pipefy adapter treats the response received by the base adapter", :type => :request do
  include RequestHelper
  
  context "the response has the expected data" do 
  it "gets the organization data" do 
    organization_data = query_object("organization")
    expect(organization_data).to eql "Pipefy Recruitment Test"
    
  end
  it "gets data from an especific pipe" do
        pipe_data = query_object("pipe")

    expect(pipe_data).to eql "Back-end Test"
  end
  it "gets the card list from a pipe" do
    card_list = query_object("cards")

    expect(card_list["data"]["cards"]["edges"][0]["node"]["id"]).to eql "4368419"
  end
  it "gets the field list from a card" do
#     field_list = query_object("card")
# 
#     expect(card_fields["data"]["cards"]["edges"][0]["node"]["id"]).to eql "4368419"
  end
  end
end