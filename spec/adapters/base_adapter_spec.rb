require 'spec_helper'
require "rails_helper"


RSpec.describe "The base adapter", :type => :request do
  include RequestHelper
  
  let(:good_response){base_adapter_request('{"query": "{ me { id name username email avatar_url created_at locale time_zone } }"}', ENV.fetch('PIPEFY_TOKEN'))}
  
  let(:bad_response){base_adapter_request('{"query": "{ me { id name username email avatar_url created_at locale time_zone } }"}', 'invalid_acess_token')}

  context "execute a post request to the Pipefy api" do 
  
    it "has the correct headers" do
      expect(good_response.request.headers).to have_key (:content_type)
      expect(good_response.request.headers).to include(:content_type => "application/json") 
      expect(good_response.request.headers).to include(:authorization => "Bearer " + ENV.fetch("PIPEFY_TOKEN"))
    end

    it "has a well formed json in the request payload" do
#       There is an Active support bug in handling json serializer which impacts on the payload method of RestClient gem. It will be fixed soon
#     https://github.com/rails/rails/issues/26132
#       valid_json?(good_response.request.payload.to_json)
    end
    it "Receives a response with a json body" do
    expect(good_response.code).eql?(200)
    expect(valid_json?(good_response.body)).to be true
    end
  end

  context "execute a post request to the Pipefy api" do 

    it "raises an error when the response does not have the 200 code" do
    expect { bad_response }.to raise_error(RestClient::Unauthorized)
    end
  end
end