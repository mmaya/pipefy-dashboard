require 'spec_helper'
require "rails_helper"


RSpec.describe "The parse api response adapter", :type => :request do
  include RequestHelper

  let(:good_response) { File.open(File.expand_path '../spec/support/fixtures/organization.json', 'rb').read }
  
    let(:response_with_errors) { File.open(File.expand_path '../spec/support/fixtures/ops.json', 'rb').read }

  context 'a json response is successfully parsed' do 

    it 'has the expected data' do
    expect(parse_response(good_response).key?('data')).to be true
    end
  end

  context 'a json response is parsed with errors' do 
    it 'raises an api response error when the response has a malformed json' do
    expect{ parse_response('')}.to raise_error(Error::ApiResponseError)
    end
    it 'raises an api response error when the response has a json without the data key' do
    expect{ parse_response(response_with_errors)}.to raise_error(Error::ApiResponseError)
    end
  end
end