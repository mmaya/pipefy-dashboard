#\ -o 0.0.0.0 -p 4569

require "rubygems"
require "bundler"

Bundler.require(:default)
Bundler.require(Sinatra::Base.environment) 

require "./spec/support/fake_pipefy_api.rb"

run Rack::URLMap.new("/" => FakePipefyApi)