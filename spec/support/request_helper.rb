#This helper creates a focal point to all the adapters calls, therefore facilitates maintenance of the tests 
module RequestHelper
  def valid_json?(json)
    begin
      JSON.parse(json)
      return true
    rescue JSON::ParserError => e
      return false
    end
  end
  
  def parse_response(response)
    PipefyAdapters::ParseApiResponse.new(response).parsed_json
  end

  def base_adapter_request (values, pipefy_acess_token)
    PipefyAdapters::BaseAdapter.execute_request(pipefy_acess_token, values)
  end
  
  def query_object(object)
    organization_id = 92858
    pipe_id = 335557
    pipefy_acess_token = ENV.fetch('PIPEFY_TOKEN')
    case object
      when "organization" then object_id = organization_id
      when "pipe" then object_id =  pipe_id
      when "cards" then object_id  = pipe_id
    end

    PipefyAdapters::PipefyAdapter.new(pipefy_acess_token).query_object(object, object_id)
  end
end