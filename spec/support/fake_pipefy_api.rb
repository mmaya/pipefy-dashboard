#\ -o 0.0.0.0 -p 4567
require 'sinatra/base'
require 'json'
require 'capybara'

class FakePipefyApi < Sinatra::Base

  def self.boot
    instance = new ()
     Capybara.server_port = '4567'
    Capybara.server_host = '0.0.0.0'
    Capybara::Server.new(instance).tap { |server| server.boot }
  end
  
  post '/' do
    authorization = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyIjp7ImlkIjo2NjE0MCwiZW1haWwiOiJwaXBlZnlkZXZyZWNydWl0aW5nZmFrZXVzZXJAbWFp'
    header_authorization = request.env["HTTP_AUTHORIZATION"] 

    if header_authorization == authorization
      payload = json_parse (request.body.read)
      json_response_file_name = payload["query"].split[1] + '.json'

      json_response(200, json_response_file_name)
    else 
      puts "unauthorized"
      json_response(401, 'ops.json')
    end
  end
  
  get "/" do
    "It works"
  end

  private
  
  def json_parse (request_body)
    begin
      JSON.parse(request_body)
      rescue JSON::ParserError => e
        $stderr.puts "Caught the exception: #{e}"
    end
  end

  def json_response(response_code, file_name)
    begin
      content_type :json
      status response_code
      File.open(File.dirname(__FILE__) + '/fixtures/' + file_name, 'rb').read
    rescue  Errno::ENOENT => e
      $stderr.puts "Caught the exception: #{e}" 
      json_response(401, 'ops.json')
    end
  end
  
#   FakePipefyApi.run!
  server = FakePipefyApi.boot
#   ENV['PIPEFY_BASE_URL'] = [server.host, server.port].join(':')
end 