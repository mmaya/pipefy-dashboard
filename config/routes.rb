Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'dashboards#index'
  get 'dashboards', to: 'dashboards#index'
  get 'dashboards/fecht_new_data', to: 'dashboards#fecht_new_data' 
end
