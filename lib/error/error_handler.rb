module Error
  module ErrorHandler
    def self.included(clazz)
      clazz.class_eval do
        rescue_from RestClient::ExceptionWithResponse, RestClient::ServerBrokeConnection, Errno::ECONNREFUSED do |e|
          respond(302, 'ConnectionRefused', 'Couldn\'t fecht the requested data from the Pipefy Api, please wait a few minutes and try again. Contact the support if the error persists.', e)
        end
        rescue_from RestClient::Forbidden, RestClient::Unauthorized do |e|
          respond(401, 'Unauthorized', 'The Pipefy api acess token is invalid.', e)
        end
        rescue_from ApiResponseError do |e|
          respond(1001, 'ApiResponseError', 'It was not possible to read the Pipefy API response, please wait a few minutes and try again. Contact the support if the error persists.', e)
          end
        end
    end

    private
    def respond(_code, _status, _message, error)
      $stderr.puts error.to_s 
      $stderr.puts error.backtrace
    flash[:error] = ("#{_code} - #{_status}: #{_message} #{error.to_s}")
    redirect_to root_path
    end
  end
end