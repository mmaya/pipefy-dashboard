class CreateOrganizations < ActiveRecord::Migration[5.1]
  def change
    create_table :organizations do |t|
      t.string :name
      t.integer :pipefy_id
      t.boolean :default, unique: true 
      t.string :encrypted_pipefy_api_token
      t.string :encrypted_pipefy_api_token_iv

      t.timestamps
    end
  end
end
