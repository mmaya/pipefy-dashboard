class CreateGraphQlQueries < ActiveRecord::Migration[5.1]
  def change
    create_table :graph_ql_queries do |t|
      t.string :name, unique: true
       t.string :value
      t.text :description
      t.text :jsonb
      t.integer :object, index:true
      
      t.timestamps
    end
  end
end
