class CreatePipes < ActiveRecord::Migration[5.1]
  def change
    create_table :pipes do |t|
      t.string :name
      t.integer :pipefy_id
      t.boolean :default
      t.belongs_to :organization, foreign_key: true
      
      t.timestamps
    end
  end
end
