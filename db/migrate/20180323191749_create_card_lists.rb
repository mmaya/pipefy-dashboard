class CreateCardLists < ActiveRecord::Migration[5.1]
  def change
    create_table :card_lists do |t|
      t.jsonb :cards, default: {}
      t.belongs_to :pipe, index: { unique: true }, foreign_key: true
      t.timestamps
    end

add_index  :card_lists, :cards, using: :gin

  end
end
