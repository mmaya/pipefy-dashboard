puts "********Seeding Data Start - Development************"
CardList.destroy_all
Pipe.destroy_all
Organization.destroy_all

organization = Organization.create(pipefy_id: "92858", name: "Pipefy Recruitment Test", pipefy_api_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ1c2VyIjp7ImlkIjo2NjE0MCwiZW1haWwiOiJwaXBlZnlkZXZyZWNydWl0aW5nZmFrZXVzZXJAbWFp", default: true)
organization.save!

puts "organization ok"

pipe = Pipe.create(pipefy_id: "335557", name: "Back-end Test", organization:organization, default:true)
pipe.save!

puts "pipe ok"

cards = PipefyAdapters::ParseApiResponse.new(File.open(File.expand_path '../spec/support/fixtures/cards.json', 'rb').read ).parsed_json

card_list = CardList.create(cards: cards, pipe: pipe)
card_list.save!

puts "Card list ok"

querie = GraphQlQuery.create(name:'organization_name', description:'querie the organization data to get its id and name', value: '{"query": "{ organization ( id: #){ name } }"}', object: 0)
querie.save!

querie = GraphQlQuery.create(name:'pipe_name', description:'querie the pipe data to get its id and name', value: '{"query": "{ pipe (id: # ) { id name } }"}', object: 1)
querie.save!

querie = GraphQlQuery.create(name:'pipe_cards', description:'querie a pipe to get its cards', value: '{"query": "{ cards ( pipe_id: #) { edges { node { id title current_phase { name } due_date fields { name value } url } } } }"}', object: 2)
querie.save!

puts 'GraphQlQuery ok'

puts "********Finshed ************"


